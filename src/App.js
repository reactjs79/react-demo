import React from 'react'
import { Route, Routes } from 'react-router-dom'
import { ToastContainer } from 'react-toast'
import './App.css'

import DashBoard from './views/Dashboard'
import Header from './shared/components/Header'
import Form from './views/Form'
import SignUp from './views/SignUp/index'
import SignIn from './views/SignIn/index'
import User from './views/User'
import ProtectedRoutes from './shared/components/ProtectedRoutes'
import UserProfile from './views/UserProfile'

function App () {
  return (
    <div>
      <Routes>
        <Route path='/' element={<SignIn />} />
        <Route path='/signup' element={<SignUp />} />
        <Route path='/dashboard' element={<Header />}>
          <Route index element={<ProtectedRoutes Component={DashBoard} />} />
        </Route>
        <Route path='/user' element={<Header />}>
          <Route index element={<ProtectedRoutes Component={User} />} />
          <Route path='add' element={<ProtectedRoutes Component={Form} />} />
          <Route path='edit' element={<ProtectedRoutes Component={Form} />} />
          <Route path=':id' element={<ProtectedRoutes Component={UserProfile} />} />
          <Route path='query' element={<ProtectedRoutes Component={UserProfile} />} />
        </Route>
      </Routes>
      <ToastContainer delay={3000} />
    </div>
  )
}

export default App
