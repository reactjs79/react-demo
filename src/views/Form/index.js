import React, { useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import { addSingleData, getSingleData, updateSingleData } from '../../shared/utils/apis/index'
import './styles.css'

const oDetailsFormat = {
  firstName: '',
  lastName: '',
  email: '',
  gender: '',
  jobTitle: '',
  profileImage: '',
  address: '',
  country: '',
  bio: '',
  phoneNumber: '',
  password: '',
  status: false
}

function Form () {
  const { state } = useLocation()
  const navigate = useNavigate()
  const [userDetails, setUserDetails] = useState(oDetailsFormat)
  const [buttonValue, setButtonValue] = useState('ADD')
  const [formValue, setFormValue] = useState('New User')

  const getSingleUser = async () => {
    const response = await getSingleData(state.data)
    setUserDetails(response.data)
  }

  useEffect(() => {
    if (state) {
      setButtonValue('SAVE')
      setFormValue('Edit')
      getSingleUser()
    }
  }, [])

  const handleInputChange = (e) => {
    let { name, value } = e.target
    if (name === 'gender' && value === 'Male') {
      value = true
    } else if (name === 'gender' && value === 'Female') {
      value = false
    }

    setUserDetails({
      ...userDetails,
      [name]: value
    })
  }

  const addNewUser = async () => {
    const response = await addSingleData(userDetails)
    console.log(response)
    navigate('/user')
  }

  const updateData = async () => {
    const response = await updateSingleData(state.data, userDetails)
    console.log(response)
    navigate('/user')
  }

  const submitHandler = (event) => {
    event.preventDefault();
    (state)
      ? updateData()
      : addNewUser()
  }

  return (
    <div className='form-container'>
      <div className="container">
        <div className="title">{formValue}</div>
        <form onSubmit={submitHandler}>
          <div className="user-details">
            <div className="input-box">
              <span className="details">First Name</span>
              <input type="text" placeholder="First name" value={userDetails.firstName} onChange={handleInputChange} name="firstName" required/>
            </div>
            <div className="input-box">
              <span className="details">Last Name</span>
              <input type="text" placeholder="Last name" value={userDetails.lastName} onChange={handleInputChange} name="lastName" required/>
            </div>
            <div className="input-box">
              <span className="details">Email</span>
              <input type="email" placeholder="Email id" value={userDetails.email} onChange={handleInputChange} name="email" required/>
            </div>
            <div className="input-box">
              <span className="details">Password</span>
              <input type="password" placeholder="Password" value={userDetails.password} onChange={handleInputChange} name="password" required/>
            </div>
            <div className="input-box">
              <span className="details">Job Title</span>
              <input type="text" placeholder="Job Title" value={userDetails.jobTitle} onChange={handleInputChange} name="jobTitle" required/>
            </div>
            <div className="input-box">
              <span className="details">Phone</span>
              <input type="text" placeholder="Phone number" maxLength={10} minLength={10} value={userDetails.phoneNumber} onChange={handleInputChange} name="phoneNumber" required/>
            </div>
            <div className="input-box">
              <span className="details">Country</span>
              <input type="text" placeholder="Country" value={userDetails.country} onChange={handleInputChange} name="country" required/>
            </div>
            <div className="input-box">
              <span className="details">Address</span>
              <input type="text" placeholder="Enter your Address" value={userDetails.address} onChange={handleInputChange} name="address" required/>
            </div>
            <div className="input-box">
              <span className="details">Profile Image</span>
              <input type="text" placeholder="Profile Image" value={userDetails.profileImage} onChange={handleInputChange} name="profileImage" required/>
            </div>
            <div className="input-box">
              <span className="details">Bio</span>
              <input type="text" placeholder="Bio" value={userDetails.bio} onChange={handleInputChange} name="bio" required/>
            </div>
          </div>
          <div className="gender-details">
            <input type="radio" name="gender" id="dot-1" value='Male' onChange={handleInputChange} required/>
            <input type="radio" name="gender" id="dot-2" value='Female' onChange={handleInputChange} required/>
            <input type="radio" name="gender" id="dot-3" value='Other' onChange={handleInputChange} required/>
            <span className="gender-title">Gender</span>
            <div className="category">
              <label htmlFor="dot-1">
                <span className="dot one"></span>
                <span className="gender">Male</span>
              </label>
              <label htmlFor="dot-2">
                <span className="dot two"></span>
                <span className="gender">Female</span>
              </label>
            </div>
          </div>
          <div className="button">
            <input type="submit" value={buttonValue}/>
          </div>
        </form>
      </div>
    </div>
  )
}

export default Form
