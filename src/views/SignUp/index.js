import React, { useState } from 'react'
import { NavLink, useNavigate } from 'react-router-dom'
import { addSingleData } from '../../shared/utils/apis/index'
import './styles.css'

const oDetailsFormat = {
  firstName: '',
  lastName: '',
  email: '',
  gender: '',
  jobTitle: '',
  profileImage: '',
  address: '',
  country: '',
  bio: '',
  phoneNumber: '',
  password: '',
  status: false
}

function SignUp () {
  const navigate = useNavigate()
  const [userDetails, setUserDetails] = useState(oDetailsFormat)

  const handleInputChange = (e) => {
    const { name, value } = e.target
    setUserDetails({
      ...userDetails,
      [name]: value
    })
  }

  function addNewUser () {
    addSingleData(userDetails)
    navigate('/')
  }

  return (
    <div className='signup-container'>
      <div className="container">
        <div className="title">Sign Up</div>
        <form onSubmit={addNewUser}>
          <div className="user-details">
            <div className="input-box">
              <span className="details">First Name</span>
              <input type="text" placeholder="First name" value={userDetails.firstName} onChange={handleInputChange} name="firstName" required/>
            </div>
            <div className="input-box">
              <span className="details">Last Name</span>
              <input type="text" placeholder="Last name" value={userDetails.lastName} onChange={handleInputChange} name="lastName" required/>
            </div>
            <div className="input-box">
              <span className="details">Email</span>
              <input type="email" placeholder="Email id" value={userDetails.email} onChange={handleInputChange} name="email" required/>
            </div>
            <div className="input-box">
              <span className="details">Password</span>
              <input type="password" placeholder="Password" value={userDetails.password} onChange={handleInputChange} name="password" required/>
            </div>
          </div>
          <div className="gender-details">
            <input type="radio" name="gender" id="dot-1" value='Male' onChange={handleInputChange} required/>
            <input type="radio" name="gender" id="dot-2" value='Female' onChange={handleInputChange} required/>
            <span className="gender-title">Gender</span>
            <div className="category">
              <label htmlFor="dot-1">
                <span className="dot one"></span>
                <span className="gender">Male</span>
              </label>
              <label htmlFor="dot-2">
                <span className="dot two"></span>
                <span className="gender">Female</span>
              </label>
            </div>
          </div>
          <div className="signup-button">
            <input type="submit" value='SignUp'/>
          </div>
          <NavLink id='link-tag' to="/signup" >Have Account?</NavLink>
        </form>
      </div>
    </div>
  )
}

export default SignUp
