import React, { useEffect, useState } from 'react'
import { getAllData, deleteSingleData, updateSingleData } from '../../shared/utils/apis/index'
import { useNavigate } from 'react-router-dom'
import './styles.css'

function User () {
  const [userData, setUserData] = useState([])
  const [userDataFilter, setUserDataFilter] = useState([])
  const [inputText, setInputText] = useState('')
  const [order, setOrder] = useState('ASC')
  const [filterData, setFilterData] = useState('all')
  const navigate = useNavigate()

  const sortingData = (col) => {
    if (order === 'ASC') {
      const sortedData = [...userData].sort((a, b) =>
        a[col].toLowerCase() > b[col].toLowerCase() ? 1 : -1
      )
      setUserData(sortedData)
      setOrder('DSC')
    } else if (order === 'DSC') {
      const sortedData = [...userData].sort((a, b) =>
        a[col].toLowerCase() < b[col].toLowerCase() ? 1 : -1
      )
      setUserData(sortedData)
      setOrder('ASC')
    }
  }

  const getAllUsers = async () => {
    const response = await getAllData()
    setUserData(response.data)
    setUserDataFilter(response.data)
  }

  useEffect(() => {
    getAllUsers()
  }, [])

  const searchHandler = (e) => {
    if (e.key === 'Enter') {
      const lowerCase = e.target.value.toLowerCase()
      setInputText(lowerCase)
    } else {
      setInputText('')
    }
  }

  useEffect(() => {
    if (inputText) {
      const filteredData = userData.filter((objectData) => {
        return objectData.firstName.toLowerCase().includes(inputText)
      })
      setUserData(filteredData)
    } else {
      getAllUsers()
    }
  }, [inputText])

  async function statusChange (element) {
    const res = await updateSingleData(element.id, { ...element, status: !element.status })
    console.log(res)
    getAllUsers()
  }

  const addUser = () => {
    navigate('/user/add')
  }
  const editData = (id) => {
    navigate('/user/edit', { state: { data: id } })
  }
  const deleteData = async (id) => {
    const response = await deleteSingleData(id)
    console.log(response)
  }
  const handler = (e) => {
    const { value } = e.target
    console.log(value);
    (value === 'true') ? setFilterData(true) : value === 'false' ? setFilterData(false) : setFilterData('all')
  }

  useEffect(() => {
    console.log(filterData)
    if (filterData !== 'all') {
      const data = userDataFilter.filter((data) => data.gender === filterData)
      setUserData(data)
    } else {
      getAllUsers()
    }
  }, [filterData])

  return (
    <div className='user-body'>
      <div className='search-container'>
        <input className='search-bar' type={'text'} placeholder='search' onKeyDown={searchHandler}></input>
        <button className='add-btn' onClick={addUser}>ADD</button>
        <select className='filter-data' value={filterData} onChange={handler}>
          <option value='all'>All</option>
          <option value={true}>Male</option>
          <option value={false}>Female</option>
        </select>
      </div>
      <table className='table-data'>
        <thead>
          <tr>
            <th>ID</th>
            <th onClick={() => sortingData('firstName')}>First Name</th>
            <th onClick={() => sortingData('lastName')}>Last Name</th>
            <th onClick={() => sortingData('email')}>Email</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {userData.map((element, index) => {
            return (
              <tr key={element.id}>
                <td>{element.id}</td>
                <td>{element.firstName}</td>
                <td>{element.lastName}</td>
                <td>{element.email}</td>
                <td><button className='status-button' style={{ backgroundColor: element.status ? '#1C6758' : '#E97777' }} onClick={() => statusChange(element)}>{(element.status) ? 'ACTIVE' : 'IN-ACTIVE'}</button></td>
                <td><button className='act-button' onClick={() => editData(element.id)}>EDIT</button>
                  <button className='act-button' onClick={() => deleteData(element.id)}>DELETE</button></td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}

export default User
