import React, { useEffect, useState } from 'react'
import { useParams, useSearchParams } from 'react-router-dom'
import { getSingleData } from '../../shared/utils/apis'
import './styles.css'

function UserProfile () {
  const { id } = useParams()
  const [user, setUser] = useState({})
  const [queryUser, setQueryUser] = useState({})
  const [searchParams] = useSearchParams({ id: 0 })
  const queryId = searchParams.get('id')

  const getUserData = async () => {
    const response = await getSingleData(id)
    setUser(response.data)
  }

  const queryData = async () => {
    const response = await getSingleData(queryId)
    setQueryUser(response.data)
  }
  useEffect(() => {
    if (id) {
      getUserData()
    } else {
      queryData()
    }
  }, [])
  return (
    <div>
      <div className='singleuser-body'>
        <h3>First Name: {user.firstName}</h3>
        <h3>Last Name: {user.lastName}</h3>
      </div>
      <div className='singleuser-body'>
        <h3>First Name: {queryUser.firstName}</h3>
        <h3>Last Name: {queryUser.lastName}</h3>
      </div>
    </div>
  )
}

export default UserProfile
