import React, { useState, useEffect } from 'react'
import { NavLink, useNavigate } from 'react-router-dom'
import { getAllData } from '../../shared/utils/apis/index'

import './styles.css'

const oDetailsFormat = {
  email: '',
  password: ''
}
function SignIn () {
  const navigate = useNavigate()
  const [userDetails, setUserDetails] = useState(oDetailsFormat)
  const [allData, setAllData] = useState()

  async function getAllUser () {
    const response = await getAllData()
    console.log(response)
    setAllData(response.data)
  }
  useEffect(() => {
    const login = sessionStorage.getItem('login')
    if (login) {
      navigate('/dashboard')
    }
    getAllUser()
  }, [])

  const handleInputChange = (e) => {
    const { name, value } = e.target
    setUserDetails({
      ...userDetails,
      [name]: value
    })
  }

  function signInUser () {
    try {
      const user = allData.find(ele => ele.email === userDetails.email)
      if (user) {
        if (allData.find(ele => ele.password === userDetails.password)) {
          sessionStorage.setItem('login', true)

          navigate('/dashboard')
        }
      } else {
        console.log('User not exist')
      }
    } catch {
      console.log('Something went wrong')
    }
  }
  return (
    <div className='login-container'>
      <div className="container">
        <div className="title">Sign In</div>
        <form onSubmit={signInUser}>
          <div className="user-details">
            <div className="input-box">
              <span className="details">Email</span>
              <input type="email" placeholder="Email id" value={userDetails.email} onChange={handleInputChange} name="email" required/>
            </div>
            <div className="input-box">
              <span className="details">Password</span>
              <input type="password" placeholder="Password" value={userDetails.password} onChange={handleInputChange} name="password" required/>
            </div>
          </div>
          <div className="signin-button">
            <input type="submit" value='Login'/>
          </div>
          <NavLink id='link-tag' to="/signup" >Register Here...</NavLink>
        </form>
      </div>
    </div>
  )
}

export default SignIn
