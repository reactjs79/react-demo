import React, { useEffect, useState } from 'react'
import { getAllData } from '../../shared/utils/apis/index'
import './styles.css'

function DashBoard () {
  const [totalUser, setTotalUser] = useState(0)
  const [maleUser, setMaleUser] = useState(0)
  const [femaleUser, setFemaleUser] = useState(0)

  const getData = async () => {
    const response = await getAllData()
    const maleCount = response.data.filter(data => data.gender === true).length
    setTotalUser(response.data.length)
    setMaleUser(maleCount)
    setFemaleUser(response.data.length - maleCount)
  }

  useEffect(() => {
    getData()
  }, [])

  return (
    <div className='dashboard-body'>
      <h3>Total: {totalUser}</h3>
      <h3>Male: {maleUser}</h3>
      <h3>Female: {femaleUser}</h3>
    </div>
  )
}

export default DashBoard
