import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { useNavigate } from 'react-router-dom'

function ProtectedRoutes (props) {
  const { Component } = props
  const navigate = useNavigate()
  useEffect(() => {
    const login = sessionStorage.getItem('login')
    if (!login) {
      navigate('/')
    }
  })
  return (
    <div>
      <Component/>
    </div>
  )
}

ProtectedRoutes.propTypes = {
  Component: PropTypes.func
}
export default ProtectedRoutes
