import React from 'react'
import './styles.css'
import { Outlet, NavLink } from 'react-router-dom'

function Header () {
  return (
    <div>
      <nav className='navbar'>
        <ul>
          <li><NavLink id='link-tag' to="/dashboard" style={({ isActive }) => ({ color: isActive ? 'black' : 'white' })}>Home</NavLink></li>
          <li><NavLink id='link-tag' to="/user" style={({ isActive }) => ({ color: isActive ? 'black' : 'white' })}>User</NavLink></li>
        </ul>
      </nav>
      <Outlet />
    </div>
  )
}

export default Header
