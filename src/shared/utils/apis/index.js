import axios from 'axios'

const userAPI = axios.create({
  baseURL: 'https://6367f297d1d09a8fa61e08ed.mockapi.io/api'
})

export async function getAllData () {
  try {
    const response = await userAPI.get('/user-data')
    if (response.status === 200) {
      return response
    } else {
      return { success: false }
    }
  } catch {
    console.log('Something went wrong')
  }
}

export async function getSingleData (id) {
  try {
    const response = await userAPI.get(`/user-data/${id}`)
    if (response.status === 200) {
      return response
    } else {
      return { success: false }
    }
  } catch {
    console.log('Something went wrong')
  }
}

export async function addSingleData (data) {
  try {
    const response = await userAPI.post('/user-data', data)
    if (response.status === 201) {
      return response
    } else {
      return { success: false }
    }
  } catch {
    console.log('Something went wrong')
  }
}
export async function updateSingleData (id, data) {
  try {
    const response = await userAPI.put(`/user-data/${id}`, data)
    if (response.status === 200) {
      return response
    } else {
      return { success: false }
    }
  } catch {
    console.log('Something went wrong')
  }
}

export async function deleteSingleData (id) {
  try {
    const response = await userAPI.delete(`/user-data/${id}`)
    if (response.status === 200) {
      return response
    } else {
      return { success: false }
    }
  } catch {
    console.log('Something went wrong')
  }
}
export { userAPI }
